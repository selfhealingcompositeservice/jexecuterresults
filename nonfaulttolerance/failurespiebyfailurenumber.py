"""
Demo of a basic pie chart plus a few additional features.

In addition to the basic pie chart, this demo shows a few optional features:

    * slice labels
    * auto-labeling the percentage
    * offsetting a slice with "explode"
    * drop-shadow
    * custom start angle

Note about the custom start angle:

The default ``startangle`` is 0, which would start the "Frogs" slice on the
positive x-axis. This example sets ``startangle = 90`` such that everything is
rotated counter-clockwise by 90 degrees, and the frog slice starts on the
positive y-axis.
"""
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

availableColors = ['lightskyblue', 'lightcoral', 'green', 'blue', 'red', 'yellow', 'orange', 'yellowgreen', 'gold']

result = read('nonfaulttolerance/nonft.execution')

failureNumbers = result.getFailureNumbersPercentage()

labels = []
sizes = []
colors = []
for i,value in enumerate(failureNumbers):
	colors.append(availableColors[i])
	sizes.append(failureNumbers[value])
	labels.append(value)

plt.pie(sizes, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=90)
# Set aspect ratio to be equal so that pie is drawn as a circle.
plt.axis('equal')

plt.show()
