import matplotlib.pyplot as plt 
import numpy as np 
import sys
sys.path.append('../')
from resultreader import read


result = read('transactional.executions')


arr = result.getFailureTimesList(True)
x = []
y = []
print arr
for i, sub_arr in enumerate(arr):
	for j, value in enumerate(sub_arr):
		x.append(i+1)
		y.append(value)

plt.xlim(0,101)
plt.scatter(x, y, s=80, facecolors='none', edgecolors='r')
plt.show()
