import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '                 TABLES                '
print '---------------------------------------'



resultNFT = read('../nonfaulttolerance/nonfaulttolerance/nonft.execution') 
resultTP = read('../transactional/transactional.executions') 
resultSH = read('../selfhealing/selfhealing.executions')

#Overhead

overhead = ['Overhead (\%)']
overhead.append(resultNFT.geTimeOverheadMean())
overhead.append(resultTP.geTimeOverheadMean())
overhead.append(resultSH.geTimeOverheadMean())

#Exceeding Time % 


exceedingTimes = ['Timeout(time) (\%)']

exceedingNFT = resultNFT.getPercentageExecutionsExceedingTime(5)

exceedingTimes.append(exceedingNFT)

exceedingTP = resultTP.getPercentageExecutionsExceedingTime(5)

exceedingTimes.append(exceedingTP)

exceedingSH = resultSH.getPercentageExecutionsExceedingTime(5)

exceedingTimes.append(exceedingSH)

#Mean Execution Time Success

meanETimes = ['Mean Time (ms)']

meanETNFT = resultNFT.getMeanTimeSuccess()
meanETTP = resultTP.getMeanTimeSuccess()
meanETSH = resultSH.getMeanTimeSuccess()

meanETimes.append(meanETNFT)
meanETimes.append(meanETTP)
meanETimes.append(meanETSH)

#Mean Execution Time Failed

meanETimesFailed = ['Mean Failed Time (ms)']

meanETNFT = resultNFT.getMeanTimeFail()
meanETTP = resultTP.getMeanTimeFail()
meanETSH = resultSH.getMeanTimeFail()

meanETimesFailed.append(meanETNFT)
meanETimesFailed.append(meanETTP)
meanETimesFailed.append(meanETSH)

#Max execution times

maxETimes = ['Max Time (ms)']
maxETimes.append(resultNFT.getMaxAccumulatedTime())
maxETimes.append(resultTP.getMaxAccumulatedTime())
maxETimes.append(resultSH.getMaxAccumulatedTime())

#Max execution times failed executions

maxETimesFailed = ['Max Time Failed (ms)']
maxETimesFailed.append(resultNFT.getMaxAccumulatedTimeFailed())
maxETimesFailed.append(resultTP.getMaxAccumulatedTimeFailed())
maxETimesFailed.append(resultSH.getMaxAccumulatedTimeFailed())

#Total Compensation Time

totalCompensationTime = ['Compensation Time (ms)']
totalCompensationTime.append('n/a')
totalCompensationTime.append(resultTP.geTotalCompensationTimeMean())
totalCompensationTime.append(resultSH.geTotalCompensationTimeMean())

#Table

table = []
table.append(overhead)
table.append(exceedingTimes)
table.append(meanETimes)
table.append(maxETimes)
table.append(meanETimesFailed)
table.append(maxETimesFailed)
table.append(totalCompensationTime)


print
print '--------- Time Comparisons  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"

