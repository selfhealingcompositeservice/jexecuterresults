from mpl_toolkits.mplot3d import Axes3D
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read
import numpy as np

result = read('nonfaulttolerance/nonft.execution')


arr = result.getFailureTimesList(True)
print arr
new_arr = []

N= result.getMaxFailures()
print 'N: ' + str(N)

for i, sub_arr in enumerate(arr):
  new_arr.append((i+1, -1, 0))
  new_arr.append((i+1, N, 0))
  for j, z in enumerate(sub_arr):
    new_arr.append((i+1, j, z))
    if (len(sub_arr) < N) & (j == N-2):
	new_arr.append((i+1, j+1, 0))
print new_arr
x, y, z = zip(*new_arr)
z = map(float, z)
grid_x, grid_y = np.mgrid[min(x):max(x):100j, min(y):max(y):100j]
grid_z = griddata((x, y), z, (grid_x, grid_y), method='cubic')

fig = plt.figure()
ax = fig.gca(projection='3d')
ax.plot_surface(grid_x, grid_y, grid_z, cmap=plt.cm.Spectral)
plt.show()
