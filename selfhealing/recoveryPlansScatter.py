import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

result = read('selfhealing.executions')

arr = result.getSHContinueTimes()
print arr
x = []
y = []

for i, sub_arr in enumerate(arr):
	for j, value in enumerate(sub_arr):
		x.append(i+1)
		y.append(value)




plt.subplot(321)
plt.scatter(x,y,s=80, c='red', marker=">")

arr = result.getSHRetryTimes()

x = []
y = []

for i, sub_arr in enumerate(arr):
	for j, value in enumerate(sub_arr):
		x.append(i+1)
		y.append(value)

plt.subplot(322)
plt.scatter(x,y,s=80, marker=(5,0))

verts = list(zip([-1.,1.,1.,-1.],[-1.,-1.,1.,-1.]))

arr = result.getSHReplicateTimes()

x = []
y = []

for i, sub_arr in enumerate(arr):
	for j, value in enumerate(sub_arr):
		x.append(i+1)
		y.append(value)

plt.subplot(323)
plt.scatter(x,y,s=80, marker=(verts,0))


arr = result.getSHCompensateTimes()

x = []
y = []
for i, sub_arr in enumerate(arr):
	for j, value in enumerate(sub_arr):
		x.append(i+1)
		y.append(float(value))


plt.subplot(324)
plt.scatter(x,y,s=80, marker=(5,1))


plt.show()
