import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

result = read('selfhealing.executions')

statesPercentages = result.getSelfhealingStatesPercentages()

labels = ['Normal','Degraded','Broken']
sizes = [statesPercentages['NORMAL'],statesPercentages['DEGRADED'],statesPercentages['BROKEN']]
colors = ['lightskyblue','yellowgreen','lightcoral']


plt.pie(sizes, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=90)
# Set aspect ratio to be equal so that pie is drawn as a circle.
plt.axis('equal')

plt.show()
