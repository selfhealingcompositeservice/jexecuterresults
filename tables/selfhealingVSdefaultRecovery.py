import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '                 TABLES                '
print '---------------------------------------'


resultSH = read('../selfhealing/selfhealing.executions')

continueR = ['Continue']
continueR.append(resultSH.getRecoveryPlanVsDefaultAverage('CONTINUE','CONTINUE'))
continueR.append(resultSH.getRecoveryPlanVsDefaultAverage('CONTINUE','RETRY'))
continueR.append(resultSH.getRecoveryPlanVsDefaultAverage('CONTINUE','REPLICATE'))
continueR.append(resultSH.getRecoveryPlanVsDefaultAverage('CONTINUE','COMPENSATE'))

retry = ['Retry']
retry.append(resultSH.getRecoveryPlanVsDefaultAverage('RETRY','CONTINUE'))
retry.append(resultSH.getRecoveryPlanVsDefaultAverage('RETRY','RETRY'))
retry.append(resultSH.getRecoveryPlanVsDefaultAverage('RETRY','REPLICATE'))
retry.append(resultSH.getRecoveryPlanVsDefaultAverage('RETRY','COMPENSATE'))

replicate = ['Replicate']
replicate.append(resultSH.getRecoveryPlanVsDefaultAverage('REPLICATE','CONTINUE'))
replicate.append(resultSH.getRecoveryPlanVsDefaultAverage('REPLICATE','RETRY'))
replicate.append(resultSH.getRecoveryPlanVsDefaultAverage('REPLICATE','REPLICATE'))
replicate.append(resultSH.getRecoveryPlanVsDefaultAverage('REPLICATE','COMPENSATE'))

compensate = ['Compensate']
compensate.append(resultSH.getRecoveryPlanVsDefaultAverage('COMPENSATE','CONTINUE'))
compensate.append(resultSH.getRecoveryPlanVsDefaultAverage('COMPENSATE','RETRY'))
compensate.append(resultSH.getRecoveryPlanVsDefaultAverage('COMPENSATE','REPLICATE'))
compensate.append(resultSH.getRecoveryPlanVsDefaultAverage('COMPENSATE','COMPENSATE'))



#Table

table = []
table.append(continueR)
table.append(retry)
table.append(replicate)
table.append(compensate)


print
print '--------- Self-healing vs default recovery  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"

