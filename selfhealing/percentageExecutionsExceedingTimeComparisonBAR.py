import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '                 TABLES                '
print '---------------------------------------'



resultNFT = read('../nonfaulttolerance/nonfaulttolerance/nonft.execution') 
resultTP = read('../transactional/transactional.executions') 
resultSH = read('selfhealing.executions')

#Exceeding Time %

exceedingTimes = ['ExTime (\%)']

exceedingNFT = resultNFT.getPercentageExecutionsExceedingTime(5)

exceedingTimes.append(exceedingNFT)

exceedingTP = resultTP.getPercentageExecutionsExceedingTime(5)

exceedingTimes.append(exceedingTP)

exceedingSH = resultSH.getPercentageExecutionsExceedingTime(5)

exceedingTimes.append(exceedingSH)

#Mean Execution Time Success

meanETimes = ['Mean ET (ms)']

meanETNFT = resultNFT.getMeanTimeSuccess()
meanETTP = resultTP.getMeanTimeSuccess()
meanETSH = resultSH.getMeanTimeSuccess()

meanETimes.append(meanETNFT)
meanETimes.append(meanETTP)
meanETimes.append(meanETSH)

#Mean Execution Time Failed

meanETimesFailed = ['Mean Failed ET (ms)']

meanETNFT = resultNFT.getMeanTimeFail()
meanETTP = resultTP.getMeanTimeFail()
meanETSH = resultSH.getMeanTimeFail()

meanETimesFailed.append(meanETNFT)
meanETimesFailed.append(meanETTP)
meanETimesFailed.append(meanETSH)

#Tale

table = []
table.append(exceedingTimes)
table.append(meanETimes)
table.append(meanETimesFailed)

print
print '--------- Time Comparisons  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"


#maxTime= resultTP.getMaxAccumulatedTime()

#print maxTime
#print ((maxTime-108286.10)*100)/float(108286.10)
