import numpy as np
import matplotlib.pyplot as plt

class ServicePrice: pass

def read(fileName):
	
	priceByService = []
	#open the file
	with open(fileName) as myfile:
	    #read each line in the file
	    for line in myfile:

		line = line.strip() #removes \n at the end of the line
		s = line.split(',')
		myvars = {}
		for a in s:
			name, var = a.split('=')
			myvars[name.strip()] = var
		if  myvars['name'] not in ('wsci','wscf'):
			price = ServicePrice()
			price.name = myvars['name']
			price.globalExpectedPrice = myvars['global_expected_price']
			price.price = myvars['price']
			priceByService.append(price)

	return priceByService

availableColors = ['lightskyblue', 'lightcoral', 'green', 'blue', 'red', 'yellow', 'orange', 'yellowgreen', 'gold']
priceByService =read('data/ehealth.price')

#a group is a service
N = len(priceByService) + 1 #number of groups. i.e, number of services + the global price
print N
ind = np.arange(N)  # the x locations for the groups
width = 0.7     # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

prices = []
xlabels = []
globalPrice = 0.0

for s in priceByService:
	globalPrice = float(s.globalExpectedPrice)
	prices.append(float(s.price))
	xlabels.append(s.name.replace("Service",""))

prices.append(globalPrice)
xlabels.append('Global Price')
print len(prices)

ax.bar(ind, prices, width, color='lightskyblue', alpha=0.5)


ax.set_ylabel('Price')
ax.set_xticks(ind+width)
ax.set_xticklabels( xlabels )


plt.show()
