import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '           STATE TRANSITION TO RECOVERY          '
print '---------------------------------------'


resultSH = read('../selfhealing/selfhealing.executions')

fromNormal = []
fromDegraded = []
fromBroken = []



normalToContinue = resultSH.getStateRecoveryPlanAverage('NORMAL','CONTINUE')
normalToRetry = resultSH.getStateRecoveryPlanAverage('NORMAL','RETRY')
normalToReplicate = resultSH.getStateRecoveryPlanAverage('NORMAL','REPLICATE')
normalToBroken = resultSH.getStateRecoveryPlanAverage('NORMAL','COMPENSATE')

fromNormal.append('Normal')
fromNormal.append(normalToContinue)
fromNormal.append(normalToRetry)
fromNormal.append(normalToReplicate)
fromNormal.append(normalToBroken)

fromDegraded = ['Degraded',resultSH.getStateRecoveryPlanAverage('DEGRADED','CONTINUE'), resultSH.getStateRecoveryPlanAverage('DEGRADED','RETRY'), resultSH.getStateRecoveryPlanAverage('DEGRADED','REPLICATE'), resultSH.getStateRecoveryPlanAverage('DEGRADED','COMPENSATE')]

fromBroken = ['Broken', resultSH.getStateRecoveryPlanAverage('BROKEN','CONTINUE'), resultSH.getStateRecoveryPlanAverage('BROKEN','RETRY'), resultSH.getStateRecoveryPlanAverage('BROKEN','REPLICATE'), resultSH.getStateRecoveryPlanAverage('BROKEN','COMPENSATE')]

#Tale

table = []
table.append(fromNormal)
table.append(fromDegraded)
table.append(fromBroken)

print
print '--------- Selfhealing States to Recovery  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"

