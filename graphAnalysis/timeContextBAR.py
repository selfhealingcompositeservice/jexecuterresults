import numpy as np
import matplotlib.pyplot as plt

class ServiceTime: pass

def read(fileName):
	
	timesByService = []
	#open the file
	with open(fileName) as myfile:
	    #read each line in the file
	    for line in myfile:

		line = line.strip() #removes \n at the end of the line
		s = line.split(',')
		myvars = {}
		for a in s:
			name, var = a.split('=')
			myvars[name.strip()] = var

		times = ServiceTime()
		times.name = myvars['name']
		times.globalExpectedTime = myvars['global_expected_time']
		times.eet = myvars['eet']
		times.timeBeforeExecution = myvars['time_before_execution']	
		times.remainingTime = myvars['remaining_time']
		times.freeTime = myvars['free_time']
		
		timesByService.append(times)

	return timesByService

availableColors = ['lightskyblue', 'lightcoral', 'green', 'blue', 'red', 'yellow', 'orange', 'yellowgreen', 'gold']
timesByService =read('data/ehealth.time')

#a group is a service
N = 9 #number of groups. i.e, number of services
ind = np.arange(N)  # the x locations for the groups
width = 0.2      # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

globalExpectedTime = []
eet = []
timeBeforeExecution = []
remainingTime = []
freeTime= []
xlabels = []
for s in timesByService:
	if s.name != 'wsci' and s.name != 'wscf':
		#globalExpectedTime.append(float(s.globalExpectedTime))
		eet.append(float(s.eet))
		timeBeforeExecution.append(float(s.timeBeforeExecution))
		remainingTime.append(float(s.remainingTime))
		freeTime.append(float(s.freeTime))
		xlabels.append(s.name.replace("Service",""))

#rects1 = ax.bar(ind, globalExpectedTime, width, color='lightskyblue', alpha=0.5)
rects2 = ax.bar(ind, eet, width, color='g', alpha=0.5)
rects3 = ax.bar(ind+width*1, timeBeforeExecution, width, color='red', alpha=0.5)
rects4 = ax.bar(ind+width*2, remainingTime, width, color='lightcoral', alpha=0.5)
rects5 = ax.bar(ind+width*3, freeTime, width, color='lightskyblue', alpha=0.5)

ax.set_ylim(ymin=0)
ax.set_ylabel('Time (ms)')
ax.set_xlabel('Service Agent')
ax.set_xticks(ind+width)
ax.set_xticklabels( xlabels )
ax.legend( (rects2[0], rects3[0],  rects4[0], rects5[0]), ('eet', 'expected', 'remains', 'available') )

def autolabel(rects):
    for rect in rects:
        h = rect.get_height()
        ax.text(rect.get_x()+rect.get_width()/2., 1.05*h, '%d'%int(h),
                ha='center', va='bottom')

#autolabel(rects1)
#autolabel(rects2)
#autolabel(rects3)
plt.grid()
plt.show()
