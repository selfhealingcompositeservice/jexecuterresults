import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '           STATE TRANSITION            '
print '---------------------------------------'


resultSH = read('../selfhealing/selfhealing.executions')

fromNormal = []
fromDegraded = []
fromBroken = []



normalToNormal = resultSH.getStateTransitionAverage('NORMAL','NORMAL')

normalToDegraded = resultSH.getStateTransitionAverage('NORMAL','DEGRADED')

normalToBroken = resultSH.getStateTransitionAverage('NORMAL','BROKEN')

fromNormal.append('Normal')
fromNormal.append(normalToNormal)
fromNormal.append(normalToDegraded)
fromNormal.append(normalToBroken)

degradedToDegraded = resultSH.getStateTransitionAverage('DEGRADED','DEGRADED')
degradedToNormal = resultSH.getStateTransitionAverage('DEGRADED','NORMAL')
degradedToBroken = resultSH.getStateTransitionAverage('DEGRADED','BROKEN')

fromDegraded.append('Degraded')
fromDegraded.append(degradedToDegraded)
fromDegraded.append(degradedToNormal)
fromDegraded.append(degradedToBroken)


brokenToBroken = resultSH.getStateTransitionAverage('BROKEN','BROKEN')
brokenToNormal = resultSH.getStateTransitionAverage('BROKEN','NORMAL')
brokenToDegraded = resultSH.getStateTransitionAverage('BROKEN','DEGRADED')


fromBroken.append('Broken')
fromBroken.append(brokenToBroken)
fromBroken.append(brokenToNormal)
fromBroken.append(brokenToDegraded)


#Tale

table = []
table.append(fromNormal)
table.append(fromDegraded)
table.append(fromBroken)

print
print '--------- Selfhealing States  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"

