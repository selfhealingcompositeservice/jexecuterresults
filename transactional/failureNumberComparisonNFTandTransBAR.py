import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

resultTransactional = read('transactional.executions')
resultNonFT = read('../nonfaulttolerance/nonfaulttolerance/nonft.execution')

xvals = range(2)
xnames=["Non fault tolerance","Transactional"]
yvals = [resultNonFT.getTotalFailuresCount(),resultTransactional.getTotalFailuresCount()]
width = 0.25
yinterval = 10

figure = plt.figure()
plt.grid(True)
plt.xlabel('Fault Tolerance')
plt.ylabel('Total Number of Failures')

plt.bar(xvals, yvals, width=width, align='center', color='red', alpha=0.5)
plt.xticks(xvals, xnames)
plt.yticks(range(0,max(yvals),yinterval))
plt.xlim([min(xvals) - 0.5, max(xvals) + 0.5])

plt.show()
