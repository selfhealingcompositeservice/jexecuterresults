import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

resultTransactional = read('transactional.executions')
resultNonFT = read('../nonfaulttolerance/nonfaulttolerance/nonft.execution')

xvals = range(3)
xnames=["Failure" , "Success","Success using retry"]
yvals = [resultNonFT.getMeanTimeFail()/float(1000),resultTransactional.getMeanTimeSuccessNoFailuresAtAll()/float(1000), resultTransactional.getMeanTimeSuccessUsingRetry()/float(1000)]
print resultNonFT.getMeanTimeFail()
print resultTransactional.getMeanTimeSuccessNoFailuresAtAll()
print resultTransactional.getMeanTimeSuccessUsingRetry()
width = 0.25
yinterval = 10

figure = plt.figure()
plt.grid(True)
plt.xlabel('Executions')
plt.ylabel('Mean Execution Time (s)')

plt.bar(xvals, yvals, width=width, align='center', color='red', alpha=0.5)
plt.xticks(xvals, xnames)
plt.yticks(range(0,max([int(i) for i in yvals]),yinterval))
plt.xlim([min(xvals) - 0.5, max(xvals) + 0.5])

plt.show()
