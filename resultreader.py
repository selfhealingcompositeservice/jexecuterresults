import numpy


class Selfhealing: pass

class Execution:
	def __str__(self):
		strObj =  'framework_et: ' + str(self.framework_et) + '\n'
		strObj += 'success: ' + str(self.success)
		return strObj

class ExecutionResult:

	def __str__(self):
		strObj =   'mean time: ' + str(self.getMeanTime()) + '\n'
		strObj +=  'mean_time_success: ' + str(self.getMeanTimeSuccess()) + '\n'
		strObj +=  'successCount: ' + str(self.getSuccessCount()) + '\n'
		strObj +=  'mean_time_fail: ' + str(self.getMeanTimeFail()) + '\n'
		strObj +=  'failCount: ' + str(self.getFailCount()) + '\n'
		strObj +=  'getFailedServicesNumberMeanInFailedExecutions ' + str(self.getFailedServicesNumberMeanInFailedExecutions()) + '\n'
		strObj +=  'getFailedProducedOutputsMean ' + str(self.getFailedProducedOutputsMean()) + '\n'
		self.printExecutions()
		return strObj

	def getGlobalEstimatedTime(self):
		for execution in self.executionList:
			return execution.globalEstimatedTime

	"""
		Prints all executions
	"""
	def printExecutions(self):
		for execution in self.executionList:
			print execution

	"""
		Returns the mean of the execution time of ALL executions without distinction
	"""
	def getMeanTime(self):
		timeSum = 0.0
		for execution in self.executionList:
			timeSum += execution.framework_et
		return timeSum/len(self.executionList)

	"""
		Returns the max accumulated time
	"""
	def getMaxAccumulatedTime(self):
		maxTime = -1.0
		for execution in self.executionList:
			if execution.success == 'true' and execution.accumulatedTime > maxTime:
				maxTime = execution.accumulatedTime
		return maxTime

	"""
		Returns the max accumulated time for failed executions
	"""
	def getMaxAccumulatedTimeFailed(self):
		maxTime = -1.0
		for execution in self.executionList:
			if execution.success == 'false' and execution.accumulatedTime > maxTime and execution.compensation == 'false':
				maxTime = execution.accumulatedTime
			else:
				if execution.success == 'false' and execution.compensationMessageTime > maxTime and execution.compensation == 'true':
					maxTime = execution.compensationMessageTime

		return maxTime

	"""
		Returns the mean of the execution time of successful executions
	"""
	def getMeanTimeSuccess(self):
		timeSum = 0.0
		count = 0
		for execution in self.executionList:
			if(execution.success=='true'):
				count+=1
				timeSum += execution.accumulatedTime
		return timeSum/count

	"""
		Returns the mean of the execution time of successful executions with no failures at all
	"""
	def getMeanTimeSuccessNoFailuresAtAll(self):
		timeSum = 0.0
		count = 0
		for execution in self.executionList:
			if(execution.success=='true') & (len(execution.failedServices) == 0):
				count+=1
				timeSum += execution.framework_et
		return timeSum/count

	"""
		Returns the mean of the execution time of successful executions using retry
	"""
	def getMeanTimeSuccessUsingRetry(self):
		timeSum = 0.0
		count = 0
		for execution in self.executionList:
			if(execution.success=='true') & (execution.retry=='true'):
				count+=1
				timeSum += execution.framework_et
		return timeSum/count


	"""
		Returns the mean of the execution time of failed executions
	"""
	def getMeanTimeFail(self):
		timeSum = 0.0
		count = 0
		for execution in self.executionList:
			if execution.success=='false' and execution.compensation == 'false':
				count+=1
				timeSum += execution.accumulatedTime
			else:
				if execution.success == 'false' and execution.compensation == 'true':
					count+=1
					timeSum += float(execution.compensationMessageTime)
		return timeSum/count

	"""
		Returns the total compensation time. Note: this time
		is not perceived by users, but runs in the background
		after the failure was reported
	"""
	def geTotalCompensationTimeMean(self):
		timeSum = 0.0
		count = 0
		for execution in self.executionList:
			if execution.success=='false' and execution.compensation == 'true':
				count+=1
				timeSum += execution.accumulatedTime
		return timeSum/count

	"""
		Returns the relative time overhead mean to
		the estimated execution time of successfull fail-free
		executions 
	"""
	def geTimeOverheadMean(self):
		return (self.getMeanTimeSuccessNoFailuresAtAll() - self.getGlobalEstimatedTime())*100/float(self.getGlobalEstimatedTime())
		
# ************************************ END TIME ************************************* "

# ************************************ COUNTS **************************************** "

	"""
		Returns the number of successful executions
	"""
	def getSuccessCount(self):
		count = 0
		for execution in self.executionList:
			if(execution.success=='true'):
				count+=1
		return count

	"""
		Returns the percentage of successful executions
	"""
	def getSuccessCountPercentage(self):
		return (self.getSuccessCount()*100)/float(len(self.executionList))

	"""
		Returns the number of total failed executions
	"""
	def getFailCount(self):
		count = 0
		for execution in self.executionList:
			if(execution.success=='false'):
				count+=1
		return count

	"""
		Returns the percentage of failed executions
	"""
	def getFailureCountPercentage(self):
		return (self.getFailCount()*100)/float(len(self.executionList))

	"""
		Returns the number of failures
	"""
	def getTotalFailuresCount(self):
		count = 0
		for execution in self.executionList:
			if len(execution.failedServices) > 0:
				count += len(execution.failedServices.values())
		return count

	"""
		Returns the number of failures of a given execution
	"""
	def getFailureCount(self, e):
		return len(e.failedServices)

	"""
		Returns the mean of failed services in failed executions
	"""
	def getFailedServicesNumberMeanInFailedExecutions(self):
		count = 0
		for execution in self.executionList:
			if(execution.success=='false'):
				count += len(execution.failedServices)
		return count/float(self.getFailCount())

	"""
		Returns the names of failed services of a given execution
	"""
	def getFailedServicesNameList(self, e):
		return e.failedServices.keys()

	"""
		Returns the produced output of a given execution
	"""
	def getProducedOutputs(self, e):
		return e.producedOutputs

	"""
		Returns the mean of produced output in failed executions
	"""
	def getFailedProducedOutputsMean(self):
		count = 0
		for execution in self.executionList:
			if(execution.success=='false'):
				for o in execution.producedOutputs:
					if o not in ('SKIP','COMPENSATE_TOKEN'):
						count += 1
		return count/float(self.getFailCount())

	def getOutputsSize(self):
		for execution in self.executionList:
			return len(execution.producedOutputs)

	"""
		Returns the percentage of produced output in failed executions
	"""
	def getFailedProducedOutputsPercentage(self):
		return (self.getFailedProducedOutputsMean()*100)/float(4)

	"""
	    Returns a bidimensional array with failure times of each execution.
	    Example output: [[223.88,2021.8],[23]]
	    If includeSuccess = True, the value [0] is added for successful executions.
	"""
	def getFailureTimesList(self,includeSuccess):
		values = []
		for execution in self.executionList:
			times = []
			if len(execution.failedServices) > 0:
				
				for tl in execution.failedServices.values():
					for v in tl:
						times.append(v)
				values.append(times)
			elif includeSuccess:
				values.append([0])
		return values

	"""
	    Gets the greatest number of failures in an execution
	"""
	def getMaxFailures(self):
		maxx = 0
		for execution in self.executionList:
			if len(execution.failedServices) > maxx:
				maxx = len(execution.failedServices)

		return int(maxx)

	"""
	    Gets the failure occurence number and its corresponding percentage of executions with that number. For example,
	    [{0,30},{2,70}] means that 30% of executions had 0 failures and 70% had 2 failures.
	"""
	def getFailureNumbersPercentage(self):
		failureNumbers = {}
		for execution in self.executionList:
			if(execution.success=='true'):
				if 0 in failureNumbers:
					failureNumbers[0] = failureNumbers[0] + 1
				else:
					failureNumbers[0] = 1
			else:
				if self.getFailureCount(execution) in failureNumbers:
					failureNumbers[self.getFailureCount(execution)] = failureNumbers[self.getFailureCount(execution)] + 1
				else:
					failureNumbers[self.getFailureCount(execution)] = 1
		for i,value in enumerate(failureNumbers):
			failureNumbers[value] = float(failureNumbers[value]*100)/float(len(self.executionList))
		return failureNumbers

	"""
	    Gets the failed services name and its corresponding percentage of failure in all executions. For example,
	    [{'serviceA',30},{'serviceB',70}]
	"""
	def getFailedServicesPercentage(self):
		failureServices = {}
		for e in self.executionList:
			for i, sName in enumerate(e.failedServices):
				if sName in failureServices:
					failureServices[sName] = failureServices[sName] + len(e.failedServices[sName])
				else:
					failureServices[sName] = len(e.failedServices[sName])

		for i,sName in enumerate(failureServices):
			failureServices[sName] = float(failureServices[sName]*100)/float(self.getFailCount())
		return failureServices

	"""
		Returns the number of generated output. Exclude control messages : ['SKIP']
	"""
	def getProducedOutputsLen(self,e):
		prodLen = 0
		for o in e.producedOutputs:
			if o != 'SKIP':
				prodLen += 1
		return prodLen

	"""
	    Returns the set of produced output percentages during failed executions. Example:
	    [{'30',40},{'20',60}] means that 30% of outputs were produced in 40% of the cases and 20% in the
	    remaining 60% of the cases
	"""
	def getProducedOutputPercentSets(self):
		outputSets = {}
		outputsize = 0
		for execution in self.executionList:
			if execution.success=='false':
				prodLen = self.getProducedOutputsLen(execution)
				outputsize = len(execution.producedOutputs)
				if prodLen in outputSets:
					outputSets[prodLen] += 1

				else:
					outputSets[prodLen] = 1

		for i,key in enumerate(outputSets):
			outputSets[(key*100)/outputsize] = outputSets.pop(key)
		return outputSets

	"""
	    Returns the set number of retries by service. Example:
	    [{'1',2},{'3',10}] means that 2 service was retried 1 times and 10 services were retried 3 times
	"""
	def getServicesRetriesSet(self):
		retrySet = {}
		for execution in self.executionList:
			if execution.retry == 'true':
				for i, s in enumerate(execution.retriedServices):
					if execution.retriedServices[s] in retrySet:
						retrySet[execution.retriedServices[s]] += 1
					else:
						retrySet[execution.retriedServices[s]] = 1
			
		return retrySet

	"""
	    Returns the mean of compensated services in compensated executions
	"""
	def getCompensatedServicesMean(self):
		count = 0
		executions = 0
		for execution in self.executionList:
			if execution.success == 'false' and execution.compensation == 'true':
				count += execution.compensatedServicesCount
				executions += 1
		return count/executions

	"""
	    Returns the mean of compensated services in compensated executions
	"""
	def getMaxCompensatedServices(self):
		count = 0
		for execution in self.executionList:
			if execution.success == 'false' and execution.compensation == 'true':
				if execution.compensatedServicesCount > count:
					count = execution.compensatedServicesCount
		return count

# ************************************************************************************************** "
	"""
	    Self-healing Methods
	"""

	"""
	    Returns the self-healing states percentages; that is, the percentage of execution which
	    stayed always in normal state, who passed to degraded and broken states.
	    
	"""
	def getSelfhealingStatesPercentages(self):
		
		statesPercentages = {}
		statesPercentages['NORMAL'] = 0
		statesPercentages['DEGRADED'] = 0
		statesPercentages['BROKEN'] = 0
	
		for execution in self.executionList:
			state = 'NORMAL'
			for analysis in execution.selfhealingData:
				if analysis.state == 'DEGRADED' and state != 'BROKEN':
					state ='DEGRADED'
				elif analysis.state == 'BROKEN':
					state ='BROKEN'

			statesPercentages[state] = statesPercentages[state] + 1
	
		statesPercentages['NORMAL'] = float((statesPercentages['NORMAL']*100))/float(len(self.executionList))
		statesPercentages['DEGRADED'] = float((statesPercentages['DEGRADED']*100))/float(len(self.executionList))
		statesPercentages['BROKEN'] = float((statesPercentages['BROKEN']*100))/float(len(self.executionList))

		return statesPercentages

	"""
	    Returns the execution times when the CONTINUE recovery was selected
	    for all executions
	    
	"""
	def getSHContinueTimes(self):
		times = []
		for execution in self.executionList:
			executionTimes = []
			for analysis in execution.selfhealingData:
				if analysis.recoveryPlan == 'CONTINUE':
					executionTimes.append(analysis.actualConsumedTime)
			times.append(executionTimes)
		return times

	"""
	    Returns the execution times when the RETRY recovery was selected
	    for all executions
	    
	"""
	def getSHRetryTimes(self):
		times = []
		for execution in self.executionList:
			executionTimes = []
			for analysis in execution.selfhealingData:
				if analysis.recoveryPlan == 'RETRY':
					executionTimes.append(analysis.actualConsumedTime)
			times.append(executionTimes)
		return times

	"""
	    Returns the execution times when the REPLICATE recovery was selected
	    for all executions
	    
	"""
	def getSHReplicateTimes(self):
		times = []
		for execution in self.executionList:
			executionTimes = []
			for analysis in execution.selfhealingData:
				if analysis.recoveryPlan == 'REPLICATE':
					executionTimes.append(analysis.actualConsumedTime)
			times.append(executionTimes)
		return times

	"""
	    Returns the execution times when the COMPENSATE recovery was selected
	    for all executions
	    
	"""
	def getSHCompensateTimes(self):
		times = []
		for execution in self.executionList:
			executionTimes = []
			for analysis in execution.selfhealingData:
				if analysis.recoveryPlan == 'COMPENSATE':
					executionTimes.append(analysis.actualConsumedTime)
			times.append(executionTimes)
		return times

	"""
		Returns the percentage of time exceeding executions using a threshold
	"""
	def getPercentageExecutionsExceedingTime(self,threshold):
		count = 0
		for execution in self.executionList:
			if(execution.success=='true' and execution.accumulatedTime > execution.globalEstimatedTime):
				percentage = (float((execution.accumulatedTime-execution.globalEstimatedTime)*100))/float(execution.globalEstimatedTime)
				if percentage > threshold:
					count+=1
		return (count*100)/float(len(self.executionList))


	"""
		Returns the average of times STATE1 went to STATE2
	"""
	def getStateTransitionAverage(self,state1,state2):
		analysisCount = 0
		count = 0
		for execution in self.executionList:
			analysis = execution.selfhealingData
			analysisCount = analysisCount + len(analysis)
			for a in analysis:
				if state1 == a.previousState and state2 == a.state:
					count = count + 1
		return (count*100)/float(analysisCount)

	"""
		Returns the average of times STATE went to RECOVERY_PLAN
	"""
	def getStateRecoveryPlanAverage(self,state,recoveryPlan):
		analysisCount = 0
		count = 0
		for execution in self.executionList:
			analysis = execution.selfhealingData
			for a in analysis:
				if state == a.state:
					analysisCount = analysisCount + 1
					if recoveryPlan == a.recoveryPlan:
						count = count + 1
		if analysisCount == 0:
			return 0
		return (count*100)/float(analysisCount)

	"""
		Returns the average of times recoveryPlan was chosed instead of defaultRecoveryPlan
	"""
	def getRecoveryPlanVsDefaultAverage(self,recoveryPlan, defaultRecoveryPlan):
		analysisCount = 0
		count = 0
		for execution in self.executionList:
			analysis = execution.selfhealingData
			for a in analysis:
				if recoveryPlan == a.recoveryPlan:
					analysisCount += 1
					if defaultRecoveryPlan == a.defaultRecoveryPlan:
						count += 1
					if defaultRecoveryPlan == 'CONTINUE' and a.defaultRecoveryPlan == 'null':
						count += 1
		if analysisCount == 0:
			return 0.0
		return (count*100)/float(analysisCount)

	"""
		Returns the percentage recoveryPlan was chosed during monitoringStage
	"""
	def getRecoveryPlanByMonitoringStateAverage(self,recoveryPlan, monitoringStage):
		monitoringStateCount = 0
		count = 0
		for execution in self.executionList:
			analysis = execution.selfhealingData
			for a in analysis:
				if monitoringStage == a.analysisType:
					monitoringStateCount += 1
					if recoveryPlan == a.recoveryPlan:
						count += 1
					if recoveryPlan == 'CONTINUE' and a.recoveryPlan == 'null':
						count += 1
		return (count*100)/float(monitoringStateCount)

	"""
		Returns the percentage of stopped executions eg: COMPENSATE when default action was RETRY
	"""
	def getStoppedExecutionsPercentageFromFailed(self):
		stopped = 0
		count = 0
		for execution in self.executionList:
			count += 1
			if execution.success=='false':
				analysis = execution.selfhealingData
				for a in analysis:
					if a.recoveryPlan == 'COMPENSATE' and a.defaultRecoveryPlan != 'COMPENSATE':
						stopped += 1

		return (stopped*100)/float(count)


	"""
		Returns the percentage of executions which decision was compensate and default decision also compensate
	"""
	def getSHFailedExecutions(self):
		stopped = 0
		count = 0
		for execution in self.executionList:
			count += 1
			if execution.success=='false':
				analysis = execution.selfhealingData
				for a in analysis:
					if a.recoveryPlan == 'COMPENSATE' and a.defaultRecoveryPlan == 'COMPENSATE':
						stopped += 1

		return (stopped*100)/float(count)

def read(fileName):
	

	executionList = []
	#open the file
	with open(fileName) as myfile:
	    #read each line in the file
	    for line in myfile:
		execution = Execution()

		line = line.strip() #removes \n at the end of the line
		s = line.split(',')
		myvars = {}
		for a in s:
			name, var = a.split('=')
			myvars[name.strip()] = var

		#global times
		execution.framework_et = float(myvars['framework_et'])
		execution.accumulatedTime = float(myvars['accumulated_et'])
		execution.globalEstimatedTime = float(myvars['eet'])

		execution.success = myvars['success']
		execution.retry = myvars['retry']
		execution.compensation = myvars['compensation']
		execution.compensationMessageTime = myvars['compensate_message_time']
		execution.totalFailures = myvars['total_failures']
		execution.compensatedServicesCount = int(myvars['compensated_services'])

		execution.successfulServicesList = myvars['successful_services_names'][1:len(myvars['successful_services_names'])-1].split(";")

		#failed services list
		s2 = myvars['failed_services_names'][1:len(myvars['failed_services_names'])-1].split(";")
		failedServices = {}

		for fs in s2:
			l = fs.split(':')
			if len(l) > 1:
				failedServices[l[0]] = [float(x) for x in l[1].split('-')]

		execution.failedServices = failedServices
		#end failed services list
	
		#retry services list
		s2 = myvars['retry_number_service'][1:len(myvars['retry_number_service'])-1].split(";")
		retriedServices = {}
		for fs in s2:
			l = fs.split(':')
			if len(l) > 1:
				retriedServices[l[0]] = int(l[1])

		execution.retriedServices = retriedServices


		#produced outputs
		execution.producedOutputs = myvars['produced_outputs'][1:len(myvars['produced_outputs'])-1].split(";")

		

		#self-healing
		selfhealingFileData = myvars['self_healing'][1:len(myvars['self_healing'])-1].split(";")
		selfhealingData = []
		if len(selfhealingFileData) > 1:
			for sh in selfhealingFileData:
				l = sh.split(':')
				shObject = Selfhealing()
				shObject.name = l[0]			
				analysisData = l[1].split('$')
				shVars = {}
				for ad in analysisData:
					sp = ad.split('&')
					shVars[sp[0]] = sp[1]
				#shObject. = shVars['']
				shObject.actualConsumedTime = shVars['actual_consumed_time']
				shObject.recoveryPlan = shVars['recovery_plan']
				shObject.localCriticalPath = shVars['local_cp']
				shObject.timeThreshold = shVars['time_threshold']
				shObject.exceedingPercentage = shVars['exceeding_percentage']
				shObject.localRemainingTime = shVars['local_remaining_time']
				shObject.localExpectedTime = shVars['local_expected_time']
				shObject.updatedLocalCriticalPath = shVars['updated_local_cp']
				shObject.defaultRecoveryPlan = shVars['default_recovery_plan']
				shObject.previousState = shVars['previous_state']
				shObject.globalEet = shVars['global_eet']
				shObject.state = shVars['state']
				shObject.analysisType = shVars['analysis_type']
				shObject.transactionalProperty = shVars['transactional_property']
				shObject.updatedFreeTime = shVars['updated_free_time']
				shObject.analysisTime = shVars['analysis_time']
				selfhealingData.append(shObject)
				execution.selfhealingData = selfhealingData

		executionList.append(execution)

 
	result = ExecutionResult()
	result.executionList = executionList
	
	return result

