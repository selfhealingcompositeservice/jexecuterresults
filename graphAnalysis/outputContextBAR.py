import numpy as np
import matplotlib.pyplot as plt

class ServiceOutputDependency: pass

def read(fileName):
	
	outputDependencyByService = []
	#open the file
	with open(fileName) as myfile:
	    #read each line in the file
	    for line in myfile:

		line = line.strip() #removes \n at the end of the line
		s = line.split(',')
		myvars = {}
		for a in s:
			name, var = a.split('=')
			myvars[name.strip()] = var
		if  myvars['name'] not in ('wsci','wscf'):
			od = ServiceOutputDependency()
			od.name = myvars['name']
			od.totalOutputs = myvars['total_outputs']
			od.outputDependency = myvars['output_dependency_number']
			outputDependencyByService.append(od)

	return outputDependencyByService

availableColors = ['lightskyblue', 'lightcoral', 'green', 'blue', 'red', 'yellow', 'orange', 'yellowgreen', 'gold']
outputDependencyByService =read('data/ehealth.work')

#a group is a service
N = len(outputDependencyByService) + 1 #number of groups. i.e, number of services + the total outputs

ind = np.arange(N)  # the x locations for the groups
width = 0.5     # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

ods = []
xlabels = []
totalOutputs = 0

for s in outputDependencyByService:
	totalOutputs = float(s.totalOutputs)
	ods.append(float(s.outputDependency))
	xlabels.append(s.name.replace("Service",""))

ods.append(totalOutputs)
xlabels.append('Total Outputs')

ax.bar(ind, ods, width, color='lightskyblue', alpha=0.5)


ax.set_ylabel('Output Dependency')
ax.set_xticks(ind+width)
ax.set_xticklabels( xlabels )


plt.show()
