import numpy as np
import matplotlib.pyplot as plt

class Work: pass

def read(fileName):
	
	workByService = []
	#open the file
	with open(fileName) as myfile:
	    #read each line in the file
	    for line in myfile:

		line = line.strip() #removes \n at the end of the line
		s = line.split(',')
		myvars = {}
		for a in s:
			name, var = a.split('=')
			myvars[name.strip()] = var
		if  myvars['name'] not in ('wsci','wscf'):
			od = Work()
			od.name = myvars['name']
			od.totalOutputs = myvars['total_outputs']
			od.outputDependency = myvars['output_dependency_number'] 
			od.allPredecessorsNumber = myvars['all_predecessors_number'] 
			workByService.append(od)

	return workByService

availableColors = ['lightskyblue', 'lightcoral', 'green', 'blue', 'red', 'yellow', 'orange', 'yellowgreen', 'gold']
workByService =read('data/ehealth.work')

#a group is a service
N = len(workByService) #number of groups. i.e, number of services

ind = np.arange(N)  # the x locations for the groups
width = 0.5     # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

ods = []
xlabels = []
wOd = 0.5
wPred = 0.5
for s in workByService:
	od = 100 - ((float(s.outputDependency)*100)/float(s.totalOutputs))
	pred = ((float(s.allPredecessorsNumber)*100)/float(N))
	workDone = od*wOd + pred*wPred
	ods.append(float(workDone))
	xlabels.append(s.name.replace("Service",""))

ax.bar(ind, ods, width, color='lightcoral', alpha=0.5)


ax.set_ylabel('Work Progression (%)')
ax.set_xticks(ind+width)
ax.set_xticklabels( xlabels )

plt.grid()
plt.show()
