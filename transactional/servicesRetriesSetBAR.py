import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

result = read('transactional.executions')

retrySet = result.getServicesRetriesSet()

xvals = range(len(retrySet))
xnames=[]
yvals = []
for i,val in enumerate(retrySet):
	xnames.append(val)
	yvals.append(retrySet[val])

width = 0.25
yinterval = 10

figure = plt.figure()
plt.grid(True)
plt.xlabel('Set of retried services')
plt.ylabel('Number of retries')

plt.bar(xvals, yvals, width=width, align='center', color='red', alpha=0.5)
plt.xticks(xvals, xnames)
plt.yticks(range(0,max([int(i) for i in yvals]),yinterval))
plt.xlim([min(xvals) - 0.5, max(xvals) + 0.5])

plt.show()
