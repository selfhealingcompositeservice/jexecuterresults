import numpy as np
import matplotlib.pyplot as plt

class Service: pass

def read(fileName):
	
	predByService = []
	#open the file
	with open(fileName) as myfile:
	    #read each line in the file
	    for line in myfile:

		line = line.strip() #removes \n at the end of the line
		s = line.split(',')
		myvars = {}
		for a in s:
			name, var = a.split('=')
			myvars[name.strip()] = var
		if  myvars['name'] not in ('wsci','wscf'):
			service = Service()
			service.name = myvars['name']
			service.allPredNumber = myvars['all_predecessors_number']
			predByService.append(service)

	return predByService

availableColors = ['lightskyblue', 'lightcoral', 'green', 'blue', 'red', 'yellow', 'orange', 'yellowgreen', 'gold']
predByService =read('data/ehealth.work')

#a group is a service
N = len(predByService) + 1 #number of groups. i.e, number of services + the total services

ind = np.arange(N)  # the x locations for the groups
width = 0.5     # the width of the bars

fig = plt.figure()
ax = fig.add_subplot(111)

preds = []
xlabels = []
totalServices = len(predByService)

for s in predByService:
	preds.append(float(s.allPredNumber))
	xlabels.append(s.name.replace("Service",""))

preds.append(totalServices)
xlabels.append('Total Services')

ax.bar(ind, preds, width, color='lightskyblue', alpha=0.5)


ax.set_ylabel('Predecessors Number')
ax.set_xticks(ind+width)
ax.set_xticklabels( xlabels )


plt.show()
