import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '                 TABLES                '
print '---------------------------------------'


resultSH = read('../selfhealing/selfhealing.executions')

fireable = ['Fireable']

fireable.append(resultSH.getRecoveryPlanByMonitoringStateAverage('CONTINUE','WHEN_FIREABLE'))
fireable.append(resultSH.getRecoveryPlanByMonitoringStateAverage('RETRY','WHEN_FIREABLE'))
fireable.append(resultSH.getRecoveryPlanByMonitoringStateAverage('REPLICATE','WHEN_FIREABLE'))
fireable.append(resultSH.getRecoveryPlanByMonitoringStateAverage('COMPENSATE','WHEN_FIREABLE'))

failure = ['Failure']

failure.append(resultSH.getRecoveryPlanByMonitoringStateAverage('CONTINUE','WHEN_FAILED'))
failure.append(resultSH.getRecoveryPlanByMonitoringStateAverage('RETRY','WHEN_FAILED'))
failure.append(resultSH.getRecoveryPlanByMonitoringStateAverage('REPLICATE','WHEN_FAILED'))
failure.append(resultSH.getRecoveryPlanByMonitoringStateAverage('COMPENSATE','WHEN_FAILED'))

fix = ['Fix']

fix.append(resultSH.getRecoveryPlanByMonitoringStateAverage('CONTINUE','WHEN_REPAIRED'))
fix.append(resultSH.getRecoveryPlanByMonitoringStateAverage('RETRY','WHEN_REPAIRED'))
fix.append(resultSH.getRecoveryPlanByMonitoringStateAverage('REPLICATE','WHEN_REPAIRED'))
fix.append(resultSH.getRecoveryPlanByMonitoringStateAverage('COMPENSATE','WHEN_REPAIRED'))



#Table

table = []
table.append(fireable)
table.append(failure)
table.append(fix)


print
print '--------- Self-healing vs default recovery  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"

