import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

print '---------------------------------------'
print '                 TABLES                '
print '---------------------------------------'



resultNFT = read('../nonfaulttolerance/nonfaulttolerance/nonft.execution') 
resultTP = read('../transactional/transactional.executions') 
resultSH = read('../selfhealing/selfhealing.executions')

#Total Failure Count

totalFailureCount = ['Total Failure Count']
totalFailureCount.append(resultNFT.getTotalFailuresCount())
totalFailureCount.append(resultTP.getTotalFailuresCount())
totalFailureCount.append(resultSH.getTotalFailuresCount())

#Max Failure Count

maxFailureCount = ['Max Failure Count']
maxFailureCount.append(resultNFT.getMaxFailures())
maxFailureCount.append(resultTP.getMaxFailures())
maxFailureCount.append(resultSH.getMaxFailures())

#Success Percentage

successPercentage = ['Successful Executions (\%)']
successPercentage.append(resultNFT.getSuccessCountPercentage())
successPercentage.append(resultTP.getSuccessCountPercentage())
successPercentage.append(resultSH.getSuccessCountPercentage())

#Failure Percentage

failurePercentage = ['Failed Executions (\%)']
failurePercentage.append(resultNFT.getFailureCountPercentage())
failurePercentage.append(resultTP.getFailureCountPercentage())
failurePercentage.append(resultSH.getSHFailedExecutions())

#Stopped Percentage

stoppedPercentage = ['Stopped Executions (\%)']
stoppedPercentage.append('n/a')
stoppedPercentage.append('n/a')
stoppedPercentage.append(resultSH.getStoppedExecutionsPercentageFromFailed())

#Mean of Compensated Services

compensatedServicesMean = ['Compensated Services Mean']
compensatedServicesMean.append('n/a')
compensatedServicesMean.append(resultTP.getCompensatedServicesMean())
compensatedServicesMean.append(resultSH.getCompensatedServicesMean())

#Max Compensated Services

maxCompensatedServices = ['Max Compensated Services']
maxCompensatedServices.append('n/a')
maxCompensatedServices.append(resultTP.getMaxCompensatedServices())
maxCompensatedServices.append(resultSH.getMaxCompensatedServices())


#getFailedProducedOutputsMean


failedProducedOutputsMean = ['Lost Outputs (\%)']
failedProducedOutputsMean.append(resultNFT.getFailedProducedOutputsPercentage())
failedProducedOutputsMean.append(resultTP.getFailedProducedOutputsPercentage())
failedProducedOutputsMean.append(resultSH.getFailedProducedOutputsPercentage())


#Table

table = []
table.append(totalFailureCount)
table.append(maxFailureCount)
table.append(successPercentage)
table.append(failurePercentage) 
table.append(stoppedPercentage)
table.append(compensatedServicesMean)
table.append(maxCompensatedServices)
table.append(failedProducedOutputsMean)


print
print '--------- Ocurrences  ---------'
print

print " \\\\\n".join([" & ".join(map(str,line)) for line in table]) + "\\\\"

