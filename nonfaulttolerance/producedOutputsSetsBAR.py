import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

resultNonFT = read('nonfaulttolerance/nonft.execution')

prodOut = resultNonFT.getProducedOutputPercentSets()

xvals = range(len(prodOut))
xnames=[]
yvals = []
for i,val in enumerate(prodOut):
	xnames.append(val)
	yvals.append(prodOut[val])

width = 0.25
yinterval = 10

figure = plt.figure()
plt.grid(True)
plt.xlabel('Percentage of generated output')
plt.ylabel('Ocurrences')

plt.bar(xvals, yvals, width=width, align='center', color='red', alpha=0.5)
plt.xticks(xvals, xnames)
plt.yticks(range(0,max([int(i) for i in yvals]),yinterval))
plt.xlim([min(xvals) - 0.5, max(xvals) + 0.5])

plt.show()
