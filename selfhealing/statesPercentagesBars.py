import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append('../')
from resultreader import read

result = read('selfhealing.executions')

statesPercentages = result.getSelfhealingStatesPercentages()

xvals = range(3)
xnames=["Normal","Degraded","Broken"]
yvals = [statesPercentages['NORMAL'],statesPercentages['DEGRADED'],statesPercentages['BROKEN']]
width = 0.25
yinterval = 10

figure = plt.figure()
plt.grid(True)
plt.xlabel('Self-healing States')
plt.ylabel('Executions (%)')

plt.bar(xvals, yvals, width=width, align='center', color='red', alpha=0.5)
plt.xticks(xvals, xnames)
plt.yticks(range(0,max([int(i) for i in yvals]),yinterval))
plt.xlim([min(xvals) - 0.5, max(xvals) + 0.5])

plt.show()
